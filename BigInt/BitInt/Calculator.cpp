#include "BigInt.h"

BigInt BigInt::to_2Complement() const {
	BigInt kq = ~(*this);

	for (int i = 0; i < M * 32; i++){
		if (kq.getBit(i) == 1)
			kq.setBit(i, 0);
		else{
			kq.setBit(i, 1);
			break;
		}
	}

	return kq;
}

BigInt BigInt::operator+(const BigInt& bi)const{
	BigInt kq;
	bool nho = 0, b1, b2;

	for (int i = 0; i < M * 32; i++){
		b1 = (*this).getBit(i);
		b2 = bi.getBit(i);

		if (b1 != b2)
			kq.setBit(i, !nho);
		else{
			kq.setBit(i, nho);
			nho = b1;
		}
	}

	return kq;
}

BigInt BigInt::operator-(const BigInt& bi)const{
	return (*this) + bi.to_2Complement();
}

//=======================================================

BigInt BigInt::operator*(const BigInt& bi)const {
	bool Q1 = 0;
	BigInt A, Q = *this;

	for (int i = 0; i < M * 32; i++)
	{
		if (Q.getBit(0) == 1){
			if (Q1 == 0) A = A - bi;
		}
		else 
			if (Q1 == 1) A = A + bi;

		Q1 = Q.getBit(0);	
		Q = Q >> 1;
		Q.setBit(M * 32 - 1, A.getBit(0));
		A = A >> 1;
	}

	return Q;
}

BigInt BigInt::operator/(const BigInt& bi)const {
	int i;
	for (i = 0; i < M; i++)
		if (bi.block[i] != 0) break;
	if (i == M) {
		cout << "Loi chia 0";
		return bi;
	}

	BigInt Q = *this, A, N = bi;
	if (N.getBit(M * 32 - 1)) N = N.to_2Complement();
	if (Q.getBit(M * 32 - 1)) Q = Q.to_2Complement();

	for (int i = 0; i < M * 32; i++)
	{
		//bool m = A.getBit(M * 32 - 2);
		A = A << 1;
		//if (!m) A.setBit(M * 32 - 1, 0);
		if (Q.getBit(M * 32 - 1)) //Nt se mat cua Q khi dich trai =1
			A.setBit(0, 1); //bo sung Nt mat cua Q vao A

		bool m = Q.getBit(M * 32 - 2);// dich khong giu dau
		Q = Q << 1;
		if (!m) Q.setBit(M * 32 - 1, 0);

		A = A - N;
		if (A.getBit(M * 32 - 1)) //neu A<0 (Nt 127 =1)
			A = A + N;
		else Q.setBit(0, 1);
	}

	if ((*this).getBit(M * 32 - 1) != bi.getBit(M * 32 - 1)) Q = Q.to_2Complement();
	return Q;
}

BigInt BigInt::operator%(const BigInt& bi)const {
	BigInt kq;
	kq = *this - *this / bi * bi;
	return kq;
}
