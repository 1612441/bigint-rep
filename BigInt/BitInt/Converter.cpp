﻿#include "BigInt.h"

void ScanBigInt(BigInt &x){
	string s;
	cin >> s;
	x.scanDEC(s);
}

void PrintBigInt(BigInt x){
	cout << x.toDEC();
}

bool* DecToBin(BigInt x){
	bool *kq = new bool[M * 32]();

	for (int i = 0; i < M * 32; i++) {
		kq[i] = x.getBit(32 * M - i - 1);
	}
	
	return kq;
}

BigInt BinToDec(bool *bit){
	BigInt kq;

	for (int i = 0; i < M * 32; i++)
		kq.setBit(i, bit[M * 32 - 1 - i]);

	return kq;
}


char* BinToHex(bool *bit){
	BigInt Dec = BinToDec(bit);
	string hex=Dec.toHEX() ;
	
	char* kq = new char[M * 8+1];
	for (int i = 0; i < M * 8; i++)
		kq[i] = toupper(hex[i]);
	kq[M * 8] = 0;
	return kq;
}


bool* HexToBin(char* hex){
	bool* bin = new bool[M * 32]();
	int k = 0; 
	unsigned char a;
	for (int i = 0; i < M * 8; i++)
	{
		if ('0' <= hex[i] && hex[i] <= '9')
			a = hex[i] - '0';
		else a = toupper(hex[i]) - 'A' + 10;

		for (int j = 0; j < 4; j++)
		{
			bin[k++] = (a & 0x08) != 0;
			a = a << 1;
		}
	}
	return bin;
}

char* DecToHex(BigInt x){
	bool* bin = DecToBin(x);
	char* hex = BinToHex(bin);
	if (!bin) delete[] bin;
	return hex;
}