﻿#include "BigInt.h"

BigInt::BigInt(){
	for (int i = 0; i < M; i++)
		block[i] = 0x00;
}

void BigInt::setBit(int pos, bool val){
	if (pos < 0 || pos >= M * 32) return;
	int t = (M - 1) - (pos / 32);
	unsigned int p = 1;
	p = p << (pos % 32);
	if (val) block[t] = block[t] | p;
	else block[t] = block[t] & (~p);
}

bool BigInt::getBit(int pos)const{
	if (pos < 0 || pos >= M * 32) return 0;
	return (block[(M - 1) - (pos / 32)] >> (pos % 32)) & 0x01;
}

BigInt BigInt::operator << (int k)const{
	if (k == 0) return *this;
	if (k < 0) return *this >> -k;
	BigInt bi = *this;
	
	bool f1 = 0, f2, neg = ((bi.block[0] & 0x80000000) != 0);
	for (int i = (M - 1); i >= 0; i--){
		f2 = (bi.block[i] & 0x80000000) != 0;

		bi.block[i] = bi.block[i] << 1;
		if (f1) bi.block[i] = bi.block[i] | 0x01;
		f1 = f2;

		bi.block[i] = bi.block[i] << k - 1;
	}
	if (neg) bi.block[0] = bi.block[0] | 0x80000000;
	return bi;
}

BigInt BigInt::operator >> (int k)const{
	if (k == 0) return *this;
	if (k < 0) return *this << -k;
	BigInt bi = *this;	

	bool f1 = 0, f2 = 0, neg = ((bi.block[0] & 0x80000000) != 0);
	for (int i = 0; i < M; i++){
		f2 = (bi.block[i] & 0x01);

		bi.block[i] = bi.block[i] >> 1;
		if (f1) bi.block[i] = bi.block[i] | 0x80000000;
		f1 = f2;

		bi.block[i] = bi.block[i] >> k - 1;
	}
	if (neg) bi.block[0] = bi.block[0] | 0x80000000;

	return bi;
}

BigInt BigInt::operator~()const{
	BigInt bi;
	for (int i = 0; i < M; i++)
		bi.block[i] = ~block[i];
	return bi;
}

BigInt BigInt::operator&(const BigInt& bi)const {
	BigInt kq;
	for (int i = 0; i < M; i++)
		kq.block[i] = block[i] & bi.block[i];
	return kq;
}

BigInt BigInt::operator|(const BigInt& bi)const{
	BigInt kq;
	for (int i = 0; i < M; i++)
		kq.block[i] = block[i] | bi.block[i];
	return kq;	
}

BigInt BigInt::operator^(const BigInt& bi)const{
	BigInt kq;
	for (int i = 0; i < M; i++)
		kq.block[i] = block[i] ^ bi.block[i];
	return kq;
}