#include "BigInt.h"

bool chia2(string& bc){
	string kq = "";
	int l = bc.length();
	char ch;
	bool nho = 0;
	for (int i = 0; i < l; i++){
		ch = '0' + (bc[i] - '0' + nho * 10) / 2;
		if (!(kq == "" && ch == '0')) kq += ch;
		if ((bc[i] - '0') % 2 != 0) nho = 1;
		else nho = 0;
	}
	if (kq == "") kq = "0";
	bc = kq;
	return nho;
}

void BigInt::DecToBigInt(string st){
	if (st == "0") return;
	bool k = chia2(st);
	DecToBigInt(st);
	*this = *this << 1;
	if (k) block[M - 1] = block[M - 1] | 0x01;
}

void BigInt::scanDEC(string st){
	for (int i = 0; i < M; i++)
		block[i] = 0x00; 

	bool neg = 0;
	if (st[0] == '-') {
		neg = 1;
		st.erase(0, 1);
	}
	DecToBigInt(st);
	if (neg) *this = (*this).to_2Complement();
}

string cong(const string& s1, const string& s2){
	int l1 = s1.length(), l2 = s2.length();
	string kq = "";
	char ch;
	bool nho = 0;
	int i;
	for (i = 1; i <= l2; i++){
		ch = s1[l1 - i] + s2[l2 - i] - 2 * '0' + nho;
		if (ch >= 10) nho = 1;
		else nho = 0;
		ch = (ch % 10) + '0';
		kq = ch + kq;
	}
	for (; i <= l1; i++){
		ch = s1[l1 - i] - '0' + nho;
		if (ch >= 10) nho = 1;
		else nho = 0;
		ch = (ch % 10) + '0';
		kq = ch + kq;
	}
	if (nho) kq = '1' + kq;
	return kq;
}

string nhan2(const string& s){
	int l = s.length();
	string kq = "";
	bool nho = 0;
	char ch;
	for (int i = l - 1; i >= 0; i--){
		ch = (s[i] - '0') * 2 + nho;
		if (ch >= 10) nho = 1;
		else nho = 0;
		ch = (ch % 10) + '0';
		kq = ch + kq;
	}
	if (nho) kq = '1' + kq;
	return kq;
}

string tru(const string& s1, const string& s2){
	int l1 = s1.length(), l2 = s2.length();
	string kq = "";
	int ch, i;
	bool nho = 0;
	for (i = 1; i <= l2; i++){
		ch = s1[l1 - i] - s2[l2 - i] - nho;
		if (ch < 0) {
			nho = 1;
			ch += 10;
		}
		else nho = 0;
		kq = (char)(ch + '0') + kq;
	}
	for (; i <= l1; i++){
		ch = s1[l1 - i] - '0' - nho;
		if (ch < 0) {
			nho = 1;
			ch += 10;
		}
		else nho = 0;
		kq = (char)(ch + '0') + kq;
	}

	int vt = 0;
	while (kq[vt] == '0') vt++;
	if (vt > 0) kq.erase(0, vt);
	return kq;
}

string BigInt::toDEC()const{
	BigInt bi = *this;
	string kq = "0", tem = "1";
	int n = M * 32;
	for (int i = 0; i < M; i++){
		if (block[i] == 0) n -= 32;
		else break;
	}

	for (int i = 1; i < n; i++){
		if ((bi.block[M - 1] & 0x01) != 0) kq = cong(tem, kq);
		bi = bi >> 1;
		tem = nhan2(tem);
	}
	if ((bi.block[M - 1] & 0x01) != 0) {
		if (n == M * 32) kq = "-" + tru(tem, kq);
		else kq = cong(tem, kq);
	}

	return kq;
}

string BigInt::toHEX()const{
	string kq = "", temp;
	for (int i = 0; i < M; i++){
		stringstream sstream;
		sstream << std::hex << block[i];
		temp = sstream.str();
		while (temp.length() < 8) temp = "0" + temp;
		kq += temp ;
	}
	return kq;
}