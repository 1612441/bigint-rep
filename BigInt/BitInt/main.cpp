#include "BigInt.h"

void main(){
	BigInt bi;
	string s;
	cout << "DEC: ";
	cin >> s;
	bi.scanDEC(s);

	cout << "10 to 2: ";
	bool* bin;
	bin = DecToBin(bi);
	for (int i = 0; i < M * 32; i++)
		cout << bin[i];
	cout << endl;
	

	char* hex = DecToHex(bi);
	cout << "10 to 16: " << hex << endl;
	cout << "2 to 16:  " << BinToHex(bin) << endl;
	
	cout << "2 to 10: " << BinToDec(bin).toDEC() << endl;

	cout << "16 to 2: ";
	bin = HexToBin(hex);
	for (int i = 0; i < M * 32; i++)
		cout << bin[i];
	cout << endl;

	delete[]bin;
	delete[]hex;

	cin.get();
	cin.get();
}

