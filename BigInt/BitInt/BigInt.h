﻿#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>
using namespace std;

#define M 4

class BigInt{
	unsigned int block[M];	//dùng M để có thể mở rộng về sau
private:	
	void DecToBigInt(string);
public:
	//file Operator.cpp
	BigInt();	
	void setBit(int pos, bool val);	//gán giá trị bit (vị trí tính từ phải qua trái)
	bool getBit(int pos)const;		//lấy giá trị bit (vị trí tính từ phải qua trái)
	
	BigInt operator~()const;				//toán tử NOT
	BigInt operator&(const BigInt&)const;	//toán tử AND
	BigInt operator|(const BigInt&)const;	//toán tử OR
	BigInt operator^(const BigInt&)const;	//toán tử XOR	
	
	BigInt operator<<(int k)const;	//dịch trái
	BigInt operator>>(int k)const;	//dịch phải	
	BigInt to_2Complement()const;	//lấy số bù 2

	//file Calculator.cpp
	BigInt operator+(const BigInt&)const;	//toán tử cộng
	BigInt operator-(const BigInt&)const;	//toán tử trừ
	BigInt operator*(const BigInt&)const;	//toán tử nhân
	BigInt operator/(const BigInt&)const;	//toán tử chia
	BigInt operator%(const BigInt&)const;

	//dùng để test kết quả tính toán
	void scanDEC(string);	//nhập số BigInt từ chuỗi số thập phân
	string toDEC()const;	//xuất BigInt ra chuỗi số thập phân (DEC)
	string toHEX()const;	//xuất BigInt ra chuỗi thập lục phân (HEX)
};

//file Converter.cpp
bool* DecToBin(BigInt x);
char* BinToHex(bool *bit);
BigInt BinToDec(bool *bit);
bool* HexToBin(char* x);
char* DecToHex(BigInt x);

void ScanBigInt(BigInt &x);	//nhập sô BigInt từ bàn phím
void PrintBigInt(BigInt x);	//in số BigInt ra màn hình